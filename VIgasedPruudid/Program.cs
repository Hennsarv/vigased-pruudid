﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace VIgasedPruudid
{
    static class Program
    {
        static int Küsi(this string küsimus)
        {
            Console.Write(küsimus + ": ");
            return int.Parse(Console.ReadLine());
        }

        static void Main(string[] args)
        {

            if (false)
            {
                Func<int, int, int> Liitmine = (x, y) => x + y;

                Action<int> Trükkimine = x => Console.WriteLine(x);

                Trükkimine(Liitmine(4, 7));

                Expression<Func<int, int>> Ruut = x => x * x;
                Console.WriteLine(Ruut);
                var r = Ruut.Compile();
                Console.WriteLine(r(4));

            }

           try
            {
                int a = "Mitu õlut".Küsi();
                int b = "Mitmekesi".Küsi();
                if (b > a) throw new Exception ("ei jagu");
                Console.WriteLine($"igaüks saab {a / b}");
            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine("miski läks nihu - vist pole joojaid");
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("miski muu jama");
                Console.WriteLine(e.Message);
                throw e;
            }

            finally
            {
                Console.WriteLine("koju tuleb ikkagi minna");
            }
        }
    }
}
